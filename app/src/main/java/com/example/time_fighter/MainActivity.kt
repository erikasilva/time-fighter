package com.example.time_fighter

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

//La clase main basicamente solo va a tener los Fragment Interaction. Aqui deben estar todos los fragment que se creen
class MainActivity : AppCompatActivity(),
    WelcomeFragment.OnFragmentInteractionListener{
    override fun onFragmentInteraction(uri: Uri) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

















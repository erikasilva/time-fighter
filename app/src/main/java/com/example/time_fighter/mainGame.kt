package com.example.time_fighter

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import java.util.zip.CheckedOutputStream


class mainGame : Fragment() {

    internal lateinit var welcomeMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button
    //FORMA 1: Problema -> no se puede tener variables private.
    @State // Hay que poner esta dependencia a todas las variables que queremos que se guarden en el estado. Con state no se puede poner private. Ya que esto guarda los estados fuera de la clase.
    var score = 0

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    @State
    var timeLeft = 10

    @State
    var gameStarted = false

    private val args: mainGameArgs  by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //FORMA 1
        StateSaver.restoreInstanceState(this, savedInstanceState)

        gameScoreTextView = view.findViewById(R.id.game_score)
        goButton = view.findViewById(R.id.go_button)
        timeLeftTextView = view.findViewById(R.id.time_left)
        //FORMA 2
        /*if (savedInstanceState != null){
            score = savedInstanceState.getInt(SCORE_KEY)
        }*/

        welcomeMessage = view.findViewById(R.id.welcome_message)
        gameScoreTextView.text = getString(R.string.score_message, score)
        timeLeftTextView.text = getString(R.string.time_left_message, score)
        val playerName = args.playerName

        welcomeMessage.text = getString(
            R.string.welcome_player,
            playerName.toString()
        )
        goButton.setOnClickListener{incrementScore()}

        if (gameStarted){
            restoreGame()
            return
        }
        resetGame()
    }

    private fun incrementScore(){ //Se ejecuta cuando se presiona el boton
        if(!gameStarted){
            startGame()
        }

        score ++
        gameScoreTextView.text = getString(R.string.score_message, score)
    }

    private fun resetGame(){
        score = 0
        gameScoreTextView.text = getString(R.string.score_message, score)
        timeLeft = 10
        timeLeftTextView.text = getString(R.string.time_left_message, timeLeft)

        countDownTimer = object: CountDownTimer(initialCountDown, countDownInterval){

            override fun onFinish() {
                endGame()
            }
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_message, timeLeft)
            }
        }
        gameStarted = false
    }

    private fun startGame(){
        countDownTimer.start()
        gameStarted = true
    }

    private fun restoreGame(){
        gameScoreTextView.text = getString(R.string.score_message, score)
        countDownTimer = object : CountDownTimer(
            timeLeft * 1000L, countDownInterval){
            override fun onFinish() {
                endGame()
            }
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left_message, timeLeft)
            }
        }
        countDownTimer.start()
    }

    private fun endGame(){
        //goButton.isClickable = false
        //Toast.makeText(this.context,"Score: $score", Toast.LENGTH_LONG).show()
        Toast.makeText(activity, getString(R.string.end_game, score), Toast.LENGTH_LONG).show() //Como estamos dentro de un fragment, se pone Activity. Caso contrario solo se pone this si se esta trabajando dentro de un activity.
        resetGame()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //FORMA 2
        //outState.putInt(SCORE_KEY, score) //En el estado se esta guardando una llave SCORE_KEY y el valor score
        //FORMA 1
        StateSaver.saveInstanceState(this, outState) // En lugar de la linea de encima. Esta clase viene con las dependencias que se instalaron en build.gradle. (Module: app)
        countDownTimer.cancel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    companion object {
        //FORMA 1
        private val SCORE_KEY = "SCORE"
    }
}
